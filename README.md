# Приложение "module-roller"

Приложение **module-roller** загружает модули и их роли в auth-service.  
Подключается к микросервисам как spring_boot_starter, добавляется автоконфигурация в spring.factories

## Добавление модулей
##### Необходимо подправить файл application.yml
##### Добавить в тег modules необходимые модули и их роли.
```
module-roller:
  modules:
  - name: module1
    title: title1
    description: description1
    authorities:
      MY_ROLE_1: ROLE_DESC_1
      MY_ROLE_2: ROLE_DESC_2
      MY_ROLE_3: ROLE_DESC_3
```

---

## Сборка и запуск приложения
##### Для сборки данного проекта необходимо в корневой директории выполнить следующую команду:
~~~
mvn clean package
~~~

##### Для запуска собранного проекта, находясь в корневой директории, необходимо выполнить команду:
~~~
java -jar /target/module-roller-1.0.jar
~~~
По выполнению данной команды приложение будет запущено на 8080 порту.  
Приложение попытается загрузить модули, и сразу же завершится **без ошибки**.  
В приложении предусмотенно логирование, по которому можно проследить если  загрузились ли модули.


Работа в Intellij IDEA
-------------------
#### Запуск проекта:
##### Run->Edit configurations->Add new configuration->Spring Boot
* Main class - выбрать Application 
* В параметрах дополнительно указать url развернутого auth-service например:  
-Dupload.url=http://localhost:5000/uaa/modules  
-Dauth.auth-token-url=http://localhost:5000/uaa/oauth/token