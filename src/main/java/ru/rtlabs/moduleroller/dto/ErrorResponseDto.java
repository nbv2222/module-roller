package ru.rtlabs.moduleroller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Дмитрий
 * @since 06.01.2018
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponseDto {

  private String timestamp;
  private int status;
  private String error;
  private String exception;
  private String message;
  private String path;
  /** для ошибок валидации */
  private Object errors;
}
