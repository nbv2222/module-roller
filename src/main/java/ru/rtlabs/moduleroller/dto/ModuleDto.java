package ru.rtlabs.moduleroller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ModuleDto {

  /** Системное имя (например: router, registries или rldd) */
  private String name;

  /** Заголовок */
  private String title;

  /** Краткое описание */
  private String description;

  /** Список доступных полномочий */
  private Map<String, String> authorities;
}
