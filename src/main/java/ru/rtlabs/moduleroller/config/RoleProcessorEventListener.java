package ru.rtlabs.moduleroller.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;
import ru.rtlabs.moduleroller.dto.ModuleDto;
import ru.rtlabs.moduleroller.upload.ModuleDictionaries;
import ru.rtlabs.moduleroller.upload.ModuleUploader;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class RoleProcessorEventListener implements ApplicationListener<ContextRefreshedEvent> {
  private final String MODULE_UPLOADER_BEAN_NAME = "moduleRollerModuleUploader";
  private final String MODULE_DICTIONARIES_BEAN_NAME = "moduleRollerModuleDictionaries";

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    // достаем из контекста необходимые бины
    ApplicationContext context = event.getApplicationContext();

    ModuleUploader moduleUploader;
    ModuleDictionaries moduleDictionary;
    try {
      moduleUploader = context.getBean(MODULE_UPLOADER_BEAN_NAME, ModuleUploader.class);
      moduleDictionary = context.getBean(MODULE_DICTIONARIES_BEAN_NAME, ModuleDictionaries.class);
    } catch (BeansException e) {
      log.error("Fatal error occurs, modules will not upload", e);
      return;
    }

    // Формируем дто module и отправляем по ресту в auth-service
    String applicationName = context.getId(); // -> spring.application.name
    Set<String> activeRoles = Optional.of(getRoles(context)).orElseGet(Collections::emptySet);
    ModuleDto dto = moduleDictionary.getModuleByAppName(applicationName, activeRoles);
    uploadModule(moduleUploader, dto);

    System.out.println();
  }

  private Set<String> getRoles(ApplicationContext context) {
    // достаем все контроллеры и схороняем их имена
    List<String> controllersName =
        Arrays.stream(context.getBeanDefinitionNames())
            .filter(s -> s.contains("Controller"))
            .collect(Collectors.toList());

    // пробегаемся по всем контроллерам
    // достаем все методы, аннотации
    // возвращаем значения аннотаций
    Set<String> roles =
        controllersName.stream()
            .map(s -> context.getBean(s))
            .map(o -> o.getClass().getMethods())
            .flatMap(
                methods ->
                    Arrays.stream(methods)
                        .map(method -> method.getDeclaredAnnotations())
                        .flatMap(
                            annotations ->
                                Arrays.stream(annotations)
                                    .filter(annotation -> annotation instanceof Secured)
                                    .map(annotation -> (Secured) annotation)
                                    .flatMap(secured -> Arrays.stream(secured.value()))))
            .collect(Collectors.toSet());
    return roles;
  }

  private void uploadModule(ModuleUploader moduleUploader, ModuleDto dto) {
    moduleUploader.uploadModule(dto);
  }
}
