package ru.rtlabs.moduleroller.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import ru.rtlabs.moduleroller.dto.ModuleDto;

import java.util.ArrayList;
import java.util.List;

@Data
@ConfigurationProperties(value = "module-roller")
public class ModuleUploadConfigurationProperties {
    public List<ModuleDto> modules = new ArrayList<>();

}
