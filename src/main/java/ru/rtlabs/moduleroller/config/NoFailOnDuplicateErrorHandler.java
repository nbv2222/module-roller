package ru.rtlabs.moduleroller.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResponseErrorHandler;
import ru.rtlabs.moduleroller.dto.ErrorResponseDto;

import java.io.IOException;

@RequiredArgsConstructor
public class NoFailOnDuplicateErrorHandler implements ResponseErrorHandler {
  private final ObjectMapper objectMapper;

  @Override
  public boolean hasError(ClientHttpResponse response) throws IOException {
    return response.getStatusCode().isError();
  }

  /** Не выбрасываем эксепшн если authService вернул ошибку, что модуль уже добавлен (duplicate) */
  @Override
  public void handleError(ClientHttpResponse response) throws IOException {
    HttpStatus statusCode = response.getStatusCode();
    if (statusCode.is5xxServerError()) {
      ErrorResponseDto errorResponseDto =
          objectMapper.readValue(response.getBody(), ErrorResponseDto.class);
      //todo потестить если в обжект маппере эксепшн вылетит
      if (errorResponseDto.getMessage() != null && !errorResponseDto.getMessage().contains("duplicate")) {
        throw new HttpServerErrorException(statusCode);
      }
    } else {
      throw new HttpClientErrorException(statusCode);
    }
  }
}
