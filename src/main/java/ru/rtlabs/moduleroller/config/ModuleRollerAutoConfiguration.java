package ru.rtlabs.moduleroller.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import ru.rtlabs.moduleroller.auth.AuthConfigurationProperties;
import ru.rtlabs.moduleroller.auth.AuthInterceptor;
import ru.rtlabs.moduleroller.auth.AuthService;
import ru.rtlabs.moduleroller.upload.ModuleDictionaries;
import ru.rtlabs.moduleroller.upload.ModuleUploader;

import java.time.Duration;
import java.util.List;

@Configuration
@EnableConfigurationProperties({
  ModuleUploadConfigurationProperties.class,
  UploadConfigurationProperties.class,
  AuthConfigurationProperties.class
})
public class ModuleRollerAutoConfiguration {

  @Bean("moduleRollerModuleUploader")
  @ConditionalOnMissingBean
  public ModuleUploader moduleUploader(
      @Qualifier("moduleRollerRestTemplate") RestTemplate restTemplate,
      UploadConfigurationProperties uploadProperties) {
    return new ModuleUploader(restTemplate, uploadProperties);
  }

  @Bean("moduleRollerModuleDictionaries")
  @ConditionalOnMissingBean
  public ModuleDictionaries dictionaries(ModuleUploadConfigurationProperties properties) {
    return new ModuleDictionaries(properties);
  }

  @Bean(name = "moduleRollerRestTemplate")
  @ConditionalOnMissingBean
  public RestTemplate restTemplate(
      RestTemplateBuilder builder,
      @Qualifier("moduleRollerAuthInterceptor") AuthInterceptor authInterceptor,
      @Qualifier("moduleRollerErrorHandler")
          NoFailOnDuplicateErrorHandler noFailOnDuplicateErrorHandler) {
    builder.setConnectTimeout(Duration.ofSeconds(30));
    builder.setReadTimeout(Duration.ofSeconds(30));

    RestTemplate build = builder.build();
    List<ClientHttpRequestInterceptor> interceptors = build.getInterceptors();
    interceptors.add(new LoggingRequestInterceptor());
    interceptors.add(authInterceptor);
    build.setInterceptors(interceptors);
    build.setErrorHandler(noFailOnDuplicateErrorHandler);
    build.setRequestFactory(
        new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));

    return build;
  }

  @Bean(name = "moduleRollerAuthService")
  @ConditionalOnMissingBean
  public AuthService authService(AuthConfigurationProperties authProperties) {
    return new AuthService(authProperties);
  }

  @Bean(name = "moduleRollerAuthInterceptor")
  @ConditionalOnMissingBean
  public AuthInterceptor authInterceptor(
      @Qualifier("moduleRollerAuthService") AuthService authService) {
    return new AuthInterceptor(authService);
  }

  @Bean(name = "moduleRollerErrorHandler")
  @ConditionalOnMissingBean
  public NoFailOnDuplicateErrorHandler errorHandler(ObjectMapper objectMapper) {
    return new NoFailOnDuplicateErrorHandler(objectMapper);
  }
}
