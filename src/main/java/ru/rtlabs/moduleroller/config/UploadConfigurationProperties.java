package ru.rtlabs.moduleroller.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("module-roller.upload")
public class UploadConfigurationProperties {
  private Integer times;
  private Integer timeout;
  private String url;
}
