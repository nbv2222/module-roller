package ru.rtlabs.moduleroller.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("module-roller.auth")
public class AuthConfigurationProperties {
  private String authTokenUrl;
  private String username;
  private String password;
  private String clientId;
  private String clientSecret;
}
