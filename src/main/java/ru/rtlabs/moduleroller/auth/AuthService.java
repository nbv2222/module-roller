package ru.rtlabs.moduleroller.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.rtlabs.moduleroller.config.LoggingRequestInterceptor;
import ru.rtlabs.moduleroller.dto.AuthTokenDto;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;

@Slf4j
public class AuthService {

  private RestTemplate restTemplate;
  private AuthConfigurationProperties properties;
  private AuthTokenDto cachedToken;

  public AuthService(AuthConfigurationProperties properties) {
    this.properties = properties;
    this.restTemplate =
        new RestTemplateBuilder()
            .requestFactory(
                () -> new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()))
            .build();

    restTemplate.setInterceptors(Collections.singletonList(new LoggingRequestInterceptor()));
  }

  public AuthTokenDto getToken() {
    if (cachedToken == null || cachedToken.isExpired()) {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
      httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
      RequestEntity<String> request =
          new RequestEntity<>(
              buildGrantTypeEntity(
                  properties.getUsername(),
                  properties.getPassword(),
                  properties.getClientId(),
                  properties.getClientSecret()),
              httpHeaders,
              HttpMethod.POST,
              URI.create(properties.getAuthTokenUrl()));
      try {
        cachedToken = restTemplate.exchange(request, AuthTokenDto.class).getBody();
      } catch (RestClientException e) {
        log.error("Error while getting auth token ", e);
      }
      log.info("New token received: {}", cachedToken);
    }
    return cachedToken;
  }

  private static String buildGrantTypeEntity(
      String userLogin, String userPassword, String clientId, String clientSecret) {
    return String.format(
        "grant_type=password&username=%s&password=%s&client_id=%s&client_secret=%s",
        userLogin, userPassword, clientId, clientSecret);
  }
}
