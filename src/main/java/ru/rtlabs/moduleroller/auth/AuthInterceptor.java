package ru.rtlabs.moduleroller.auth;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import ru.rtlabs.moduleroller.dto.AuthTokenDto;

import java.io.IOException;

public class AuthInterceptor implements ClientHttpRequestInterceptor {

  private static final String AUTHORIZATION_HEADER = "Authorization";

  private AuthService authService;

  public AuthInterceptor(AuthService authService) {
    this.authService = authService;
  }

  @Override
  public ClientHttpResponse intercept(
      HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

    AuthTokenDto token = authService.getToken();
    if (token != null) {
      request.getHeaders().set(AUTHORIZATION_HEADER, "Bearer " + token.getAccessToken());
    }

    return execution.execute(request, body);
  }
}
