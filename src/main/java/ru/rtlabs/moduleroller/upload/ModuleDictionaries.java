package ru.rtlabs.moduleroller.upload;

import lombok.Data;
import ru.rtlabs.moduleroller.config.ModuleUploadConfigurationProperties;
import ru.rtlabs.moduleroller.dto.ModuleDto;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class ModuleDictionaries {
  public static final String DEFAULT_ROLE_DESCRIPTION = " DEFAULT ROLE DESCRIPTION";
  public static final String DEFAULT_TITLE = " DEFAULT TITLE";
  public static final String DEFAULT_DESCRIPTION = " DEFAULT DESCRIPTION";

  private static final Character ROLE_DELIMITER = '_';

  private final ModuleUploadConfigurationProperties properties;

  public ModuleDto getModuleByAppName(String applicationName, Set<String> activeRoles) {
    Set<String> rolesWithoutPrefix =
        activeRoles.stream().map(s -> s.substring(secondIndexOf(s))).collect(Collectors.toSet());

    return properties.getModules().stream()
        .filter(dto -> dto.getName().equalsIgnoreCase(applicationName))
        .peek(
            dto -> {
              // Пытаемся связать роли приложения с известными нам ролями
              Map<String, String> recognizedRoles =
                  dto.getAuthorities().entrySet().stream()
                      .filter(s -> rolesWithoutPrefix.contains(s.getKey()))
                      .collect(Collectors.toMap(s -> s.getKey(), s -> s.getValue()));
              // Если какие-то из ролей нам не известны, кладем их как есть
              rolesWithoutPrefix.forEach(
                  s -> recognizedRoles.putIfAbsent(s, s + DEFAULT_ROLE_DESCRIPTION));

              dto.setAuthorities(recognizedRoles);
            })
        .findFirst()
        .orElseGet(() -> buildModuleAsIs(applicationName, rolesWithoutPrefix));
  }

  private ModuleDto buildModuleAsIs(String applicationName, Set<String> rolesWithoutPrefix) {
    String toUpperCaseName = applicationName.toUpperCase();
    return ModuleDto.builder()
        .name(toUpperCaseName)
        .title(toUpperCaseName + DEFAULT_TITLE)
        .description(toUpperCaseName + DEFAULT_DESCRIPTION)
        .authorities(
            rolesWithoutPrefix.stream()
                .collect(Collectors.toMap(o -> o, s -> s + DEFAULT_ROLE_DESCRIPTION)))
        .build();
  }

  private int secondIndexOf(String s) {
    int firstDelimiter = s.indexOf(ROLE_DELIMITER) + 1;
    int secondDelimiter = s.substring(firstDelimiter).indexOf(ROLE_DELIMITER) + 1;
    return firstDelimiter + secondDelimiter;
  }
}
