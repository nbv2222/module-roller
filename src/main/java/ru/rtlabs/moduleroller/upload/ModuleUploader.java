package ru.rtlabs.moduleroller.upload;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;
import ru.rtlabs.moduleroller.config.UploadConfigurationProperties;
import ru.rtlabs.moduleroller.dto.ModuleDto;

@Data
@Slf4j
@RequiredArgsConstructor
public class ModuleUploader {
  private final RestTemplate restTemplate;
  private final UploadConfigurationProperties uploadProperties;

  @SneakyThrows
  public void uploadModule(ModuleDto dto) {

    for (int i = 0; i < uploadProperties.getTimes(); i++) {
      try {
        restTemplate.postForEntity(uploadProperties.getUrl(), dto, String.class);

        // if no exception thrown = great! All modules are persist correctly
        break;
      } catch (Exception e) {
        log.error("Error occurs when loading modules", e);
        Thread.sleep(uploadProperties.getTimeout() * 1000);
      }
    }
  }
}
