package ru.rtlabs.moduleroller.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import ru.rtlabs.moduleroller.auth.AuthConfigurationProperties;
import ru.rtlabs.moduleroller.auth.AuthInterceptor;
import ru.rtlabs.moduleroller.auth.AuthService;
import ru.rtlabs.moduleroller.upload.ModuleDictionaries;
import ru.rtlabs.moduleroller.upload.ModuleUploader;

@Configuration
@EnableConfigurationProperties({
  AuthConfigurationProperties.class,
  UploadConfigurationProperties.class,
  ModuleUploadConfigurationProperties.class
})
@EnableAutoConfiguration(exclude = ModuleRollerAutoConfiguration.class)
public class CommonTestConfiguration {

  @Bean
  public AuthService authService(AuthConfigurationProperties properties) {
    return new AuthService(properties);
  }

  @Bean
  public ModuleDictionaries moduleDictionaries(ModuleUploadConfigurationProperties properties) {
    return new ModuleDictionaries(properties);
  }

  @Bean
  public ModuleUploader moduleUploader(
      RestTemplate restTemplate, UploadConfigurationProperties properties) {
    return new ModuleUploader(restTemplate, properties);
  }

  @Bean
  public AuthInterceptor authInterceptor(AuthService authService) {
    return new AuthInterceptor(authService);
  }

  @Bean
  public NoFailOnDuplicateErrorHandler errorHandler(ObjectMapper objectMapper) {
    return new NoFailOnDuplicateErrorHandler(objectMapper);
  }

  @Bean
  public RestTemplate restTemplate(
      RestTemplateBuilder builder,
      AuthInterceptor authInterceptor,
      NoFailOnDuplicateErrorHandler errorHandler) {
    return builder.interceptors(authInterceptor).errorHandler(errorHandler).build();
  }

  @Bean
  public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(
      ObjectMapper objectMapper) {
    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setObjectMapper(objectMapper);
    return converter;
  }

  @Bean
  public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
    return builder.failOnUnknownProperties(false).failOnEmptyBeans(false).build();
  }
}
