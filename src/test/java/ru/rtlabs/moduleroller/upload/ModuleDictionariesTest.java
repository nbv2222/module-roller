package ru.rtlabs.moduleroller.upload;

import org.junit.BeforeClass;
import org.junit.Test;
import ru.rtlabs.moduleroller.config.ModuleUploadConfigurationProperties;
import ru.rtlabs.moduleroller.dto.ModuleDto;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static ru.rtlabs.moduleroller.upload.ModuleDictionaries.DEFAULT_DESCRIPTION;
import static ru.rtlabs.moduleroller.upload.ModuleDictionaries.DEFAULT_ROLE_DESCRIPTION;
import static ru.rtlabs.moduleroller.upload.ModuleDictionaries.DEFAULT_TITLE;

public class ModuleDictionariesTest {

  private static ModuleUploadConfigurationProperties properties;
  private Set<String> existingRole = Stream.of("ROLE_TESTAPP_MY_ROLE").collect(Collectors.toSet());
  private Set<String> twoExistedRoles =
      Stream.of("ROLE_TESTAPP_MY_ROLE", "ROLE_TESTAPP_MY_ROLE_TWO").collect(Collectors.toSet());
  private Set<String> nonExistingRole = Collections.emptySet();

  @BeforeClass
  public static void init() {
    properties = new ModuleUploadConfigurationProperties();
    properties.setModules(
        Collections.singletonList(
            ModuleDto.builder()
                .name("app-name")
                .description("app-desc")
                .title("app-title")
                .authorities(Collections.singletonMap("MY_ROLE", "MY_ROLE_DESC"))
                .build()));
  }

  @Test
  public void getModuleByExistingAppName_shouldReturn_filledModule() {
    ModuleDictionaries moduleDictionaries = new ModuleDictionaries(properties);
    ModuleDto moduleByAppName = moduleDictionaries.getModuleByAppName("app-name", existingRole);

    assertEquals("Application name should be same", "app-name", moduleByAppName.getName());
    assertEquals(
        "Application description should be filled", "app-desc", moduleByAppName.getDescription());
    assertEquals("Application title should be filled", "app-title", moduleByAppName.getTitle());
    assertEquals(
        "Application role should be filled",
        "MY_ROLE_DESC",
        moduleByAppName.getAuthorities().values().stream().findFirst().get());
  }

  @Test
  public void getModuleByExistingAppName_withTwoRoles_shouldReturn_filledModule() {
    String mySecondRole = "MY_ROLE_TWO";

    ModuleDictionaries moduleDictionaries = new ModuleDictionaries(properties);
    ModuleDto moduleByAppName = moduleDictionaries.getModuleByAppName("app-name", twoExistedRoles);

    assertEquals("Application name should be same", "app-name", moduleByAppName.getName());
    assertEquals(
        "Application description should be filled", "app-desc", moduleByAppName.getDescription());
    assertEquals("Application title should be filled", "app-title", moduleByAppName.getTitle());
    assertEquals(
        "Application role should be filled",
        mySecondRole + DEFAULT_ROLE_DESCRIPTION,
        moduleByAppName.getAuthorities().get(mySecondRole));
  }

  @Test
  public void getModuleByNonExistingAppName_shouldReturn_defaultModule() {
    String nonExistingAppName = "non-existing-app-name";
    ModuleDictionaries moduleDictionaries = new ModuleDictionaries(properties);
    ModuleDto moduleByAppName =
        moduleDictionaries.getModuleByAppName(nonExistingAppName, nonExistingRole);

    assertTrue(
        "Application name should be same",
        "non-existing-app-name".equalsIgnoreCase(moduleByAppName.getName()));
    assertTrue(
        "Application description should be default",
        (nonExistingAppName + DEFAULT_DESCRIPTION)
            .equalsIgnoreCase(moduleByAppName.getDescription()));
    assertTrue(
        "Application title should be default",
        (nonExistingAppName + DEFAULT_TITLE).equalsIgnoreCase(moduleByAppName.getTitle()));
    assertTrue("Application role should be null", moduleByAppName.getAuthorities().isEmpty());
  }
}
