package ru.rtlabs.moduleroller.upload;

import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import ru.rtlabs.moduleroller.config.CommonTestConfiguration;
import ru.rtlabs.moduleroller.dto.ModuleDto;

import java.nio.charset.Charset;
import java.util.Collections;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

/**
 * В этом классе проверяем как отработает отправка модуля Так же в этом классе проверяем как
 * обработается ответ errorHandler'ом
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = CommonTestConfiguration.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ModuleUploaderTest {
  @Value("${module-roller.upload.url}")
  private String uploadUrl;

  @Autowired private RestTemplate restTemplate;
  @Autowired private ModuleUploader uploader;

  private MockRestServiceServer mockServer;

  @Before
  @SneakyThrows
  public void init() {
    restTemplate.setInterceptors(
        Collections.emptyList()); // обнуляем интерцепторы, что бы их не тестить
    mockServer = MockRestServiceServer.createServer(restTemplate);
  }

  @Test
  @SneakyThrows
  public void addModule_ReturnSuccess() {
    mockServer
        .expect(requestTo(uploadUrl))
        .andExpect(method(HttpMethod.POST))
        .andRespond(
            withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(
                    IOUtils.toString(
                        getClass().getResourceAsStream("/successfully_added_module.json"),
                        Charset.defaultCharset())));

    uploader.uploadModule(ModuleDto.builder().build());
    // no exception thrown
  }

  @Test
  @SneakyThrows
  public void addModule_withExistedError_shouldThrowNoException() {
    mockServer
        .expect(requestTo(uploadUrl))
        .andExpect(method(HttpMethod.POST))
        .andRespond(
            withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(
                    IOUtils.toString(
                        getClass().getResourceAsStream("/duplicate_added_module.json"),
                        Charset.defaultCharset())));

    uploader.uploadModule(ModuleDto.builder().build());
    // no exception thrown
  }

  @Test
  @SneakyThrows
  public void addModule_withSomeError_shouldThrowNoException() {
    mockServer
        .expect(requestTo(uploadUrl))
        .andExpect(method(HttpMethod.POST))
        .andRespond(
            withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(
                    IOUtils.toString(
                        getClass().getResourceAsStream("/some_error_added_module.json"),
                        Charset.defaultCharset())));

    uploader.uploadModule(ModuleDto.builder().build());
    // no exception thrown
  }

  @Test
  @SneakyThrows
  public void addModule_withInvalidResponseStructure_shouldThrowNoException() {
    mockServer
            .expect(requestTo(uploadUrl))
            .andExpect(method(HttpMethod.POST))
            .andRespond(
                    withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(
                                    IOUtils.toString(
                                            getClass().getResourceAsStream("/invalid_response_add_module.json"),
                                            Charset.defaultCharset())));

    uploader.uploadModule(ModuleDto.builder().build());
    // no exception thrown
  }
}
