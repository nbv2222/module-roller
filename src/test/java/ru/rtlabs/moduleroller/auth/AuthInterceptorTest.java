package ru.rtlabs.moduleroller.auth;

import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import ru.rtlabs.moduleroller.config.CommonTestConfiguration;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.Collections;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = CommonTestConfiguration.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthInterceptorTest {
  @Value("${module-roller.auth.auth-token-url}")
  private String ssoUrl;

  public final String AUTH_TOKEN_HEADER = "Authorization";
  private final String ssoContentType = "application/x-www-form-urlencoded";
  private final String authTokenValue = "Bearer f91cf6b6-b782-4e62-bcec-15a32db22f5c";
  private final String testUrl = "http://www.test.com";

  @Autowired private RestTemplate restTemplate;
  @Autowired private AuthService authService;
  private MockRestServiceServer mockServer;
  private MockRestServiceServer ssoMockServer;

  @Before
  @SneakyThrows
  public void init() {
    mockServer = MockRestServiceServer.createServer(restTemplate);
    Field privateField = authService.getClass().getDeclaredField("restTemplate");
    privateField.setAccessible(true);
    RestTemplate restTemplate = (RestTemplate) privateField.get(authService);
    restTemplate.setInterceptors(
        Collections.emptyList()); // убираем логгинг интерцептор для тестов.
    ssoMockServer = MockRestServiceServer.createServer(restTemplate);
  }

  @Test
  public void check_InterceptorAddAuthToken_asHeader() throws IOException {
    ssoMockServer
        .expect(requestTo(ssoUrl))
        .andExpect(method(HttpMethod.POST))
        .andExpect(content().contentType(ssoContentType))
        .andRespond(
            withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(
                    IOUtils.toString(
                        getClass().getResourceAsStream("/sso_token.json"),
                        Charset.defaultCharset())));

    mockServer
        .expect(requestTo(testUrl))
        .andExpect(method(HttpMethod.GET))
        .andExpect(header(AUTH_TOKEN_HEADER, authTokenValue))
        .andRespond(withStatus(HttpStatus.OK));

    restTemplate.getForEntity(testUrl, String.class);
  }
}
