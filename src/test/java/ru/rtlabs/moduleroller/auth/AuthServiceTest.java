package ru.rtlabs.moduleroller.auth;

import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import ru.rtlabs.moduleroller.config.CommonTestConfiguration;
import ru.rtlabs.moduleroller.dto.AuthTokenDto;

import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.Collections;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = CommonTestConfiguration.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthServiceTest {
  @Value("${module-roller.auth.auth-token-url}")
  private String ssoUrl;

  private final String tokenFromResponse = "f91cf6b6-b782-4e62-bcec-15a32db22f5c";
  private final String contentType = "application/x-www-form-urlencoded";
  private final Long expiresIn = 3600L;

  @Autowired private AuthService authService;

  private MockRestServiceServer mockServer;

  @Before
  @SneakyThrows
  public void init() {
    Field privateField = authService.getClass().getDeclaredField("restTemplate");
    privateField.setAccessible(true);
    RestTemplate restTemplate = (RestTemplate) privateField.get(authService);
    restTemplate.setInterceptors(
        Collections.emptyList()); // убираем логгинг интерцептор для тестов.
    mockServer = MockRestServiceServer.createServer(restTemplate);
  }

  @Test
  @SneakyThrows
  public void getToken_ReturnSuccess() {
    mockServer
        .expect(requestTo(ssoUrl))
        .andExpect(method(HttpMethod.POST))
        .andExpect(content().contentType(contentType))
        .andRespond(
            withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(
                    IOUtils.toString(
                        getClass().getResourceAsStream("/sso_token.json"),
                        Charset.defaultCharset())));

    AuthTokenDto token = authService.getToken();
    Assert.assertEquals(token.getAccessToken(), tokenFromResponse);
    Assert.assertNotNull(token.getExpiresIn());
  }

  @Test
  @SneakyThrows
  public void getTokenTwice_returnCachedToken() {
    // mock one request
    mockServer
        .expect(requestTo(ssoUrl))
        .andExpect(method(HttpMethod.POST))
        .andExpect(content().contentType(contentType))
        .andRespond(
            withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(
                    IOUtils.toString(
                        getClass().getResourceAsStream("/sso_token.json"),
                        Charset.defaultCharset())));

    AuthTokenDto token = authService.getToken();
    Assert.assertEquals(token.getAccessToken(), tokenFromResponse);
    Assert.assertNotNull(token.getExpiresIn());

    AuthTokenDto token1 = authService.getToken();
    Assert.assertEquals(token, token1);
  }

  @Test
  public void check_isTokenExpire_returnFalse() {
    AuthTokenDto token = new AuthTokenDto(tokenFromResponse, expiresIn, System.currentTimeMillis());
    Assert.assertFalse(token.isExpired());
  }

  @Test
  @SneakyThrows
  public void check_isTokenExpire_returnTrue() {
    AuthTokenDto token =
        new AuthTokenDto(
            tokenFromResponse, expiresIn, System.currentTimeMillis() - expiresIn * 1000);

    Assert.assertTrue(token.isExpired());
  }
}
